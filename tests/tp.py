# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if n == 0:
        return "0"
    else:
        return "S" * n + "0"


def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    elif b == "0":
        return a
    else:
        while a.startswith("S"):
            a = a[1:]
            b = "S" + b
        return b



def multiplication(a: str, b: str) -> str:
    result = "0"
    while a.startswith("S"):
        result = addition(result, b)
        a = a[1:]
    return result


def facto_ite(n: int) -> int:
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    a, b = 0, 1
    for _ in range(n):
        a, b = b, a + b
    return a



def golden_phi(n: int) -> int:
    return fibo_ite(n + 1) / fibo_ite(n)


def sqrt5(n: int) -> int:
    x = 1
    for _ in range(n):
        x = (x + 5 / x) / 2
    return x


def pow(a: float, n: int) -> float:
    result = 1
    current_power = a

    while n > 0:
        if n % 2 == 1:
            result *= current_power
        current_power *= current_power
        n //= 2

    return result
