#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }
  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  size_t max_len = lhs_len > rhs_len ? lhs_len : rhs_len;

  char *result = (char *)calloc(max_len + 2, sizeof(char));
  unsigned int carry = 0;

  for (size_t i = 0; i < max_len; i++) {
    unsigned int lhs_digit = i < lhs_len ? get_digit_value(lhs[lhs_len - i - 1]) : 0;
    unsigned int rhs_digit = i < rhs_len ? get_digit_value(rhs[rhs_len - i - 1]) : 0;

    if (VERBOSE) {
      fprintf(stderr, "add: lhs_digit=%u, rhs_digit=%u, carry=%u\n", lhs_digit, rhs_digit, carry);
    }

    unsigned int sum = lhs_digit + rhs_digit + carry;
    carry = sum / base;
    unsigned int result_digit = sum % base;

    result[max_len - i] = to_digit(result_digit);

    if (VERBOSE) {
      fprintf(stderr, "add: result[%zu]=%c\n", max_len - i, result[max_len - i]);
    }
    // Fill the function, the goal is to compute lhs + rhs
    // You should allocate a new char* large enough to store the result as a
    // string Implement the algorithm Return the result
  }

  if (carry > 0) {
    result[0] = to_digit(carry);
  } else {
    memmove(result, result + 1, max_len + 1);
  }

  return drop_leading_zeros(result);
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  size_t max_len = lhs_len > rhs_len ? lhs_len : rhs_len;

  char *result = (char *)calloc(max_len + 1, sizeof(char));
  int borrow = 0;

  for (size_t i = 0; i < max_len; i++) {
    int lhs_digit = i < lhs_len ? (int)get_digit_value(lhs[lhs_len - i - 1]) : 0;
    int rhs_digit = i < rhs_len ? (int)get_digit_value(rhs[rhs_len - i - 1]) : 0;

    if (VERBOSE) {
      fprintf(stderr, "sub: lhs_digit=%d, rhs_digit=%d, borrow=%d\n", lhs_digit, rhs_digit, borrow);
    }

    int diff = lhs_digit - rhs_digit - borrow;
    if (diff < 0) {
      diff += base;
      borrow = 1;
    } else {
      borrow = 0;
    }

    result[max_len - i - 1] = to_digit(diff);

    if (VERBOSE) {
      fprintf(stderr, "sub: result[%zu]=%c\n", max_len - i - 1, result[max_len - i - 1]);
    }
  }
  if (borrow == 1) {
	  return NULL;
  } else {
  	return drop_leading_zeros(result);
  }
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t result_length = lhs_length + rhs_length;

  char *result = malloc(result_length+1);
  memset(result,'0',result_length);
  result[result_length] = '\0';
  unsigned int produit;
  size_t carry;
  int k;
  unsigned int digit;
  for (int i = rhs_length - 1; i >= 0; i--)
  {
	  for (int j = lhs_length - 1;j >= 0; j--)
	  {
		  produit = get_digit_value(lhs[j]) * get_digit_value(rhs[i]);
		  carry = 0;
		  k = i + j + 1;
		  if (VERBOSE) {
			  fprintf(stderr, "mul: digit %c digit %c carry %zu\n", lhs[j], rhs[i], carry); 
		  }
		  while (produit > 0 || carry > 0) {
			  digit = get_digit_value(result[k]) + carry + (produit % base);
			  carry = digit / base;
			  result[k] = to_digit(digit % base);
			  produit /= base;
			  k--;
		  }
		  if (VERBOSE) {
			  fprintf(stderr, "mul: result: digit %c carry %zu\n", result[k+1], carry);
		  }
	  }
  }

  return drop_leading_zeros(result);
}


// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
